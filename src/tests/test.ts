import { TestCase, TestModule, gondola, importData } from "gondolajs";
import Commons from '../helper/Commons';
import { SearchPlace } from '../data/SearchPlace';
import HomePage from '../pages/HomePage'
import GeneralPage from "../pages/GeneralPage";
import SearchResultsPage from "../pages/SearchResultsPage";
import { FilterHotel } from "../data/FilterHotel";
TestModule("test");
Before(async () => {
    if(await Commons.getPlatForm() == "chrome"){
        await GeneralPage.navigateTo();
    }

})

let currentDate = new Date();
let mondayNextWeek = Commons.getDateOfNextWeek(currentDate, 1)
currentDate = new Date();
let wednesdayNextWeek = Commons.getDateOfNextWeek(currentDate, 3);
var searchInfo = new SearchPlace("Da Nang", mondayNextWeek, wednesdayNextWeek, 2, 2, 2, 13, 4);

let filter = new FilterHotel("500000", "1000000", 3);

TestCase("Agoda_TC001, Search and sort hotel successfully", async () => {
    await HomePage.searchHotelsHomes(searchInfo);
    //await HomePage.waitPageSearchResult();
    await SearchResultsPage.checkAreaExist(searchInfo.place);
    await SearchResultsPage.sortLowestPrice();
});

// TestCase("Agoda_TC002, Search and filter hotel successfully", async () => {
//     await HomePage.searchHotelsHomes(searchInfo);
//     await HomePage.waitPageSearchResult();
//     await SearchResultsPage.checkAreaExist(searchInfo.place);
//     await SearchResultsPage.filterHotel(filter);
//     await SearchResultsPage.checkPriceAndStarFilteredHighlight("₫ " + filter.priceBoxMin + " - ₫ " + filter.priceBoxMax + "", filter.starRate);
//     await SearchResultsPage.checkAreaExist(searchInfo.place);
//     await SearchResultsPage.checkPriceAndStar(filter);
// });


