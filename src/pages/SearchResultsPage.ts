import { gondola, locator, page, action, KeyCode } from "gondolajs";
import { BasePage } from "../helper/BasePage";
import { FilterHotel } from "../data/FilterHotel";
import Commons from "../helper/Commons";

@page
export class SearchResultsPage extends BasePage {
    @locator private areaCityName = async (index: number, destination?: string) => {
        return {
            xpath: "(//span[@class='areacity-name']/span[contains(text(), '" + destination + "')])[" + index.toString() + "]",
            android: "(//*[@resource-id='com.agoda.mobile.consumer:id/label_ssr_hotelname'])[" + index.toString() + "]"
        }
    };
    @locator private hotelName = async (name: string) => {
        return {
            android: "//*[@resource-id='com.agoda.mobile.consumer:id/label_ssr_hotelname' and @text='" + name + "']"
        }
    };
    @locator private lowestPrice = {
        xpath: "//a[@data-element-name='search-sort-price']",
        android: "//*[@text='Lowest price']/.."
    };
    @locator private actualPrice = async (index: number) => {
        return {
            xpath: "//div[@data-element-index='" + index.toString() + "']//*[@id='actual-price']",
            android: ""
        }
    };
    @locator private priceFilter = {
        xpath: "//div[@data-component='PriceFilterRange']",
        android: "",
    };
    @locator private priceBoxMin = {
        xpath: "//div[@data-element-name='search-filter-pricerange-searchbox']/input[@id='price_box_0']",
        android: "",
    }
    @locator private priceBoxMax = {
        xpath: "//div[@data-element-name='search-filter-pricerange-searchbox']/input[@id='price_box_1']",
        android: "",
    }
    @locator private starratingFilter = {
        xpath: "//div[@data-component='StarRating']",
        android: "",
    }
    @locator private starratingFilterReact = (star: number) => {
        return {
            xpath: '//span[@class="filter-item-info StarRating-' + star + ' "]//span[@class="checkbox-icon"]',
            android: "",
        }
    }
    @locator private priceHighlighted = (price: string) => {
        return {
            xpath: "//button[@class='btn PillDropdown__Button PillDropdown__Button--select']//span[contains(text(),'" + price + "')]",
            android: "",
        }
    }
    @locator private starFilterHighlighted = (star: number) => {
        return {
            xpath: '//div[@data-element-name="search-filter-type-starrating"]//button[@class="btn PillDropdown__Button PillDropdown__Button--select"]//span[text()="' + star + '"]',
            android: "",
        }
    }
    @locator private priceActuaHotel = {
        xpath: "//span[@id='actual-price']",
    }
    @locator private starAreaLine = (index: number) => {
        return {
            xpath: "(//div[@class='starAreaLine']//i[@data-selenium='hotel-star-rating'])[" + index + "]"
        }
    }
    @locator private priceHotel = (index: number) => {
        return {
            xpath: "(//span[@class='price-box__price__amount'])[" + index + "]",
            android: "(//*[@resource-id='com.agoda.mobile.consumer:id/textView_search_hotel_price'])[" + index + "]"
        }
    }
    @locator private actualPriceText = (price: string) => { 
        return {
            xpath: "",
            android: "//*[@resource-id='com.agoda.mobile.consumer:id/textView_search_hotel_price' and @text='"+ price +"']"
        }
    };
    @locator private bestMatchButton = { xpath: "//*[@text='Best match']/../.." }

    public async checkAreaExist(destination: string) {
        let listdata: string[] = Array(5);
        let platform = await Commons.getPlatForm();
        if (platform == "chrome") {
            for (var i = 1; i <= listdata.length; i++) {
                let check = await gondola.doesControlExist(await this.areaCityName(i, destination));
                let areaCityName = await gondola.getText(await this.areaCityName(i, destination));
                if (check) {
                    console.log(i + ". Search Result is displayed correctly - Destination: " + destination);
                    console.log(areaCityName);
                }
            }
        }
        if (platform == "android") {
            for (var i = 1; i <= listdata.length; i++) {
                await Commons.swipeUntilElementAppreared(await this.areaCityName(i), 0, 400);
                console.log(listdata.length + ". Search Result is displayed correctly - Destination: " + destination);
                let hotelName = await gondola.getText(await this.areaCityName(i));
                await Commons.swipeUntilElementDisappreared(await this.hotelName(hotelName), 0, -400);
                i = 0;
                listdata.length -= 1;
            }
        }
    }
    public async sortLowestPrice() {
        let platform = await Commons.getPlatForm();
        if (platform == "android") {
            await gondola.tap(this.bestMatchButton);
        }
        await this.clickElement(this.lowestPrice);
        let index: number[] = Array(5);
        let listPrice: number[] = Array(5);
        var isSorted = true;

        if (platform == "chrome") {
            for (var i = 0; i < listPrice.length; i++) {
                listPrice[i] = parseInt((await gondola.getText(this.priceHotel(i + 1))).replace(",", ""));
                if (listPrice[i] > listPrice[i + 1]) {
                    isSorted = false;
                    break;
                }
                console.log(listPrice[i]);
            }
            console.log(isSorted);
        }
        if (platform == "android") {
            for (var i = 1; i <= index.length; i++) {
                await Commons.swipeUntilElementAppreared(this.priceHotel(i), 0, 400);
                let price = await gondola.getText(this.priceHotel(i));
                listPrice.push(parseInt(price.replace(/(đ |¥ )/g, "").replace(",", "")));
                await Commons.swipeUntilElementDisappreared(this.actualPriceText(price), 0, -400);
                i = 0;
                index.length -= 1;
            }
            for (let j = 0; j < listPrice.length; j++) {
                if (listPrice[j] > listPrice[j + 1]) {
                    isSorted = false;
                    break;
                }
                console.log(listPrice[j]);
            }
            console.log(isSorted);
        }
    }

    public async filterHotel(filter: FilterHotel) {
    await this.clickElement(this.priceFilter);
    await gondola.enter(this.priceBoxMin, filter.priceBoxMin);
    await gondola.enter(this.priceBoxMax, filter.priceBoxMax);
    await gondola.pressKey(KeyCode.Enter);
    await this.clickElement(this.starratingFilter);
    await this.clickElement(this.starratingFilterReact(filter.starRate));
}

    public async checkPriceAndStarFilteredHighlight(priceRange: string, starRate: number) {
    let currentPrice = await gondola.doesControlExist(this.priceHighlighted(priceRange));
    await gondola.checkEqual(currentPrice, true);
    let currentStar = await gondola.doesControlExist(this.starFilterHighlighted(starRate));
    await gondola.checkEqual(currentStar, true);
}

    public async checkPriceAndStar(filter: FilterHotel) {
    let check = await gondola.doesControlExist(this.priceActuaHotel);
    if (check == true) {
        await gondola.scrollTo(this.priceActuaHotel);
    }
    let flag: boolean = true;
    await gondola.scrollTo(this.lowestPrice);
    for (var i = 1; i < 6; i++) {
        let temp = await gondola.getElementAttributes(this.starAreaLine(i), ["class"]);
        var str = '';
        for (var p in temp) {
            if (temp.hasOwnProperty(p)) {
                str += p + '::' + temp[p] + '\n';
            }
        }
        if (str.includes("3") || str.includes("3.5") || str.includes("4") || str.includes("4.5") || str.includes("5")) {
            flag = true;
        }
        else {
            flag = false;
            break;
        }
        let price = await gondola.getText(this.priceHotel(i))
        let priceForm = +price.split(",").join("");
        if (priceForm < +filter.priceBoxMin || priceForm > +filter.priceBoxMax) {
            flag = false;
            break;
        }
        await gondola.scrollTo(this.priceHotel(i))
    }
    await gondola.checkEqual(flag, true);
}
}

export default new SearchResultsPage();