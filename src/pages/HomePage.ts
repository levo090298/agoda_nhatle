import { gondola, locator, page, action } from "gondolajs";
import { BasePage } from "../helper/BasePage";
import { Constant } from "../Data/Constant";
import Commons from "../helper/Commons";
import { SearchPlace } from "../data/SearchPlace";

@page
export class HotelsAndHomesPage extends BasePage {
    @locator private searchTextbox = {
        xpath: "//input[@data-selenium='textInput']",
        android: "//*[@resource-id='com.agoda.mobile.consumer:id/textbox_textsearch_searchbox']",
    };
    @locator private firstOptionAutocomplete = {
        xpath: "(//ul[@class='AutocompleteList']/li)[1]",
        android: "(//androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout)[1]",
    };
    @locator private captionDatePickerByDate = async (caption: string) => {
        return {
            xpath: "//div[@class='DayPicker-Caption']/div[.='" + caption + "']",
            android: "",
        }
    };
    @locator private nextMonthButton = {
        xpath: "//span[@aria-label='Next Month']",
        android: "",
    };
    @locator private dateByCaptionDatePicker = async (caption: string, date: number) => {
        return {
            xpath: "//div[@class='DayPicker-Caption']/div[.='" + caption + "']/../..//span[.='" + date + "']",
            android: "//android.widget.TextView[@content-desc='" + date + " " + caption + "']",
        }
    };
    @locator private familyTravelerOptions = {
        xpath: "//div[.='Family travelers']",
        android: "",
    };
    @locator private numberOfObject = async (object: string) => {
        return {
            xpath: "(//span[contains(., '" + object + "')]/../span)[2]",
            android: "//*[@resource-id='com.agoda.mobile.consumer:id/" + object.toLowerCase() + "_selector']//*[@resource-id='com.agoda.mobile.consumer:id/amount']",
        }
    };
    @locator private minusOfObject = async (object: string) => {
        return {
            xpath: "(//span[contains(., '" + object + "')]/../span)[1]",
            android: "//*[@resource-id='com.agoda.mobile.consumer:id/" + object.toLowerCase() + "_selector']//*[@resource-id='com.agoda.mobile.consumer:id/minus']",
        }
    };
    @locator private plusOfObject = async (object: string) => {
        return {
            xpath: "(//span[contains(., '" + object + "')]/../span)[4]",
            android: "//*[@resource-id='com.agoda.mobile.consumer:id/" + object.toLowerCase() + "_selector']//*[@resource-id='com.agoda.mobile.consumer:id/plus']",
        }
    };
    @locator private searchButton = {
        xpath: "//button[@data-selenium='searchButton']",
        android: "//*[@resource-id='com.agoda.mobile.consumer:id/button_home_search']",
    };
    @locator private childAgeDropdownMenu = async (index: number) => {
        return {
            xpath: "//ul[@class='ChildAges__dropdown']/li[" + index.toString() + "]",
            android: "//*[@text ='Child " + index.toString() + "']/..//*[contains(@resource-id, 'spinner_child_age')]"
        }
    };
    @locator private childAgeDropdownMenuItemList = async (indexChild: number, indexChildList: number) => {
        return {
            xpath: "//ul[@class='ChildAges__dropdown']/li[" + indexChild.toString() + "]//option[@value='" + indexChildList.toString() + "']",
            android: "//android.widget.TextView[@text='" + indexChildList.toString() + "']"
        }
    };
    @locator private popupContent = {
        xpath: "//div[@class='ModalLoadingSpinner__content']",
    };

    @locator private gotItButton = { id: "com.agoda.mobile.consumer:id/got_it" };
    @locator private allowButton = { id: "com.android.packageinstaller:id/permission_allow_button" };
    @locator private notNowButton = { xpath: "//*[@text = 'Not now']" };

    @locator private homeSearchTextBox = { id: "com.agoda.mobile.consumer:id/textbox_home_textsearch" };
    @locator private datepickerPanel = { id: "com.agoda.mobile.consumer:id/panel_home_datepicker" };
    @locator private doneButton = { id: "com.agoda.mobile.consumer:id/button_datepicker_done" };
    @locator private occupancyPanel = { id: "com.agoda.mobile.consumer:id/panel_home_occupancy" };
    @locator private preferFamilyRoomCheckBox = { id: "com.agoda.mobile.consumer:id/preferFamilyRoomCheckBox" };
    @locator private occupancyconfirmButton = { id: "com.agoda.mobile.consumer:id/occupancy_confirm" };

    public async checkHomePageDisplayed() {
        gondola.checkWindowExist("Agoda | Booking Over 2 Million Hotels and Homes & Flights");
    }

    private async setPickerDate(date: Date) {
        let platform = await Commons.getPlatForm();
        var monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"];
        if (platform === "chrome") {
            let expectedCaptionDatePicker = monthNames[date.getMonth()] + " " + date.getFullYear().toString();
            // Check true month, if not click next month button
            let checkCaption = await gondola.doesControlExist(await this.captionDatePickerByDate(expectedCaptionDatePicker));
            while (!checkCaption) {
                await this.clickElement(this.nextMonthButton);
                gondola.wait(1);
                checkCaption = await gondola.doesControlExist(await this.captionDatePickerByDate(expectedCaptionDatePicker));
            }
            // Click date
            await this.clickElement(await this.dateByCaptionDatePicker(expectedCaptionDatePicker, date.getDate()));
        }
        else if (platform === "android") {
            let expectedCaptionDatePicker = monthNames[date.getMonth()] + " " + date.getFullYear().toString();
            gondola.tap(await this.dateByCaptionDatePicker(expectedCaptionDatePicker, date.getDate()));
        }
    }

    public async searchHotelsHomes(searchInfo: SearchPlace) {
        let platform = await Commons.getPlatForm();
        if (platform === "chrome") {
            // add place
            await this.sendText(this.searchTextbox, searchInfo.place);
            await this.clickElement(this.firstOptionAutocomplete);
            await gondola.waitForClickable(this.nextMonthButton, Constant.MEDIUM_TIME_WAIT_ELEMENT);

            // choose check-in date
            await this.setPickerDate(searchInfo.checkInDate);

            // choose check-out date
            await this.setPickerDate(searchInfo.checkOutDate);

            // choose number of room
            await this.clickElement(this.familyTravelerOptions);

            // select guest detail
            await this.selectGuestDetails(searchInfo);

            //search
            await this.clickElement(this.searchButton);
        }
        if (platform === "android") {
            gondola.tap(this.gotItButton);
            gondola.tap(this.allowButton);
            let check = await gondola.doesControlExist(this.notNowButton);
            if (check) {
                gondola.tap(this.notNowButton);
            }
            gondola.tap(this.homeSearchTextBox);
            // add place
            await this.sendText(this.searchTextbox, searchInfo.place);
            gondola.tap(this.firstOptionAutocomplete);
            gondola.tap(this.datepickerPanel);

            // choose check-in date
            await this.setPickerDate(searchInfo.checkInDate);

            // choose check-out date
            await this.setPickerDate(searchInfo.checkOutDate);
            gondola.tap(this.doneButton);

            // choose select guest detail
            let check1 = await gondola.doesControlExist(this.occupancyPanel);
            if (check1) {
                gondola.tap(this.occupancyPanel);
            }
            await this.selectGuestDetails(searchInfo);
            gondola.tap(this.preferFamilyRoomCheckBox);
            gondola.tap(this.occupancyconfirmButton);

            // click search button
            gondola.tap(this.searchButton);
        }
    }

    private async selectGuestDetails(searchInfo: SearchPlace) {
        let currentNumberRooms = parseInt(await gondola.getText(await this.numberOfObject("Room")));
        while (currentNumberRooms != searchInfo.rooms) {
            if (currentNumberRooms < searchInfo.rooms) {
                await this.clickElement(await this.plusOfObject("Room"));
            }
            else {
                await this.clickElement(await this.minusOfObject("Room"));
            }
            currentNumberRooms = parseInt(await gondola.getText(await this.numberOfObject("Room")));
        }

        // choose number of adults
        let currentNumberAdults = parseInt(await gondola.getText(await this.numberOfObject("Adult")));
        while (currentNumberAdults != searchInfo.adults) {
            if (currentNumberAdults < searchInfo.adults) {
                await this.clickElement(await this.plusOfObject("Adult"));
            }
            else {
                await this.clickElement(await this.minusOfObject("Adult"));
            }
            currentNumberAdults = parseInt(await gondola.getText(await this.numberOfObject("Adult")));
        }
        // choose number of children
        let currentNumberChildren = parseInt(await gondola.getText(await this.numberOfObject("Children")));
        while (currentNumberChildren != searchInfo.children) {
            if (currentNumberChildren < searchInfo.children) {
                await this.clickElement(await this.plusOfObject("Children"));
            }
            else {
                await this.clickElement(await this.minusOfObject("Children"));
            }
            currentNumberChildren = parseInt(await gondola.getText(await this.numberOfObject("Children")));
        }

        //select ages of children
        let platform = await Commons.getPlatForm();
        for (let i = 0; i < searchInfo.arr.length; i++) {
            await this.clickElement(await this.childAgeDropdownMenu(i+1));
            if (platform === "android") {
                await this.scrollToElement(await this.childAgeDropdownMenuItemList(i+1, searchInfo.arr[i]), await this.childAgeDropdownMenuItemList(i+1, 13), 0, 500);
            }
            await this.clickElement(await this.childAgeDropdownMenuItemList(i+1, searchInfo.arr[i]));
        }
    }

    public async scrollToElement(ele: any, from: any, toX: number, toY: number) {
        let check = await gondola.doesControlExist(ele);
        console.log(check);
        
        let bound = await gondola.getElementBounds(from);
        let eleX = bound.left;
        let eleY = bound.top;
        while (!check) {
            gondola.swipeByCoordinates(eleX, eleY, toX, toY);
            check = await gondola.doesControlExist(ele);
            console.log(check);
        }
    }

    public async waitPageSearchResult() {
        gondola.waitForDisappear(this.popupContent);
    }

}

export default new HotelsAndHomesPage();