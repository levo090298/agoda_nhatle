import { gondola } from "gondolajs";

export class Commons{
    public getDateOfNextWeek(date: Date, dayOfWeek: number){
        date.setDate(date.getDate() + (dayOfWeek + 7 - date.getDay()) % 7);
        return date;
    }

    public async getPlatForm()  {
        let capabilities = await gondola.getCapabilities();
        let platformBrowser = capabilities["browserName"];
        if (platformBrowser == undefined){
            return capabilities.platformName;
        }
        return platformBrowser
    }

    public async scrollToElement(ele: any, from: any, toX: number, toY: number) {
        let check = await gondola.doesControlExist(ele);
        let bound = await gondola.getElementBounds(from);
        let eleX = bound.left;
        let eleY = bound.top;
        while (!check){
            gondola.swipeByCoordinates(eleX, eleY, toX, toY);
            check = await gondola.doesControlExist(ele);
        }
    }

    public async swipeUntilElementAppreared(ele: any, toX: number, toY: number) {
        let check = await gondola.doesControlExist(ele);
        // console.log(check + " Appreared");
        let screenSize = await gondola.getDeviceScreenSize();
        let screenSizeX = (screenSize.width / 2);
        let screenSizeY = (screenSize.height / 2);
        while (!check){
            gondola.swipeByCoordinates(screenSizeX, screenSizeY, toX, toY);
            check = await gondola.doesControlExist(ele);
            // console.log(check + "while - Appreared");
        }
    }

    public async swipeUntilElementDisappreared(ele: any, toX: number, toY: number) {
        let check = await gondola.doesControlExist(ele);
        // console.log(check + " Disappreared");
        let screenSize = await gondola.getDeviceScreenSize();
        let screenSizeX = (screenSize.width / 2);
        let screenSizeY = (screenSize.height / 2);
        while (check){
            gondola.swipeByCoordinates(screenSizeX, screenSizeY, toX, toY);
            check = await gondola.doesControlExist(ele);
            // console.log(check + "while - Disappreared");
        }
    }
}
export default new Commons();