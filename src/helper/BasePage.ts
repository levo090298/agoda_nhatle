import { action, gondola } from "gondolajs";
import { Constant } from "../Data/Constant";
import Commons from "../helper/Commons"

export class BasePage {

    @action("click element", "Click element when element is clickable")
    protected async clickElement(element: ILocator | string) {
        await gondola.waitForElement(element, Constant.SHORT_TIME_WAIT_ELEMENT);
        let platform = await Commons.getPlatForm();
        if (platform == "chrome") {
            await gondola.click(element);
        }
        if (platform == "android") {
            await gondola.tap(element);
        }
    }

    @action("send text", "Send text to element when element exists")
    protected async sendText(element: ILocator | string, text: string) {
        await gondola.waitForElement(element, Constant.SHORT_TIME_WAIT_ELEMENT);
        await gondola.enter(element, text);
    }

    @action("move mouse to control")
    protected async moveMouseToControl(element: ILocator | string) {
        await gondola.waitForElement(element, Constant.SHORT_TIME_WAIT_ELEMENT);
        await gondola.moveMouse(element);
    }

    @action("select item")
    protected async selectItem(list: string | ILocator, item: string) {
        await gondola.waitForClickable(list, Constant.SHORT_TIME_WAIT_ELEMENT);
        await gondola.select(list, item);
    }

    @action("navigate to", "Navigate to dashboard testarchitect")
    public async navigateTo() {
        await gondola.navigate(Constant.URL);
    }

    public async getPlatForm() {
        let capabilities = await gondola.getCapabilities();
        let platformBrowser = capabilities["browserName"];
        if (platformBrowser == undefined) {
            return capabilities.platformName;
        }
        return platformBrowser
    }

}
export default new BasePage();