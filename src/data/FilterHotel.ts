export class FilterHotel {
    priceBoxMin: string;
    priceBoxMax: string;
    starRate: number;

    constructor(priceBoxMin: string, priceBoxMax: string,starRate: number ){
        this.priceBoxMin = priceBoxMin;
        this.priceBoxMax = priceBoxMax;
        this.starRate = starRate;
    }
}