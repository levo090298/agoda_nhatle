export namespace  Constant {

    //Time wait
    export const SHORT_TIME_WAIT_ELEMENT: number = 10;
    export const MEDIUM_TIME_WAIT_ELEMENT: number = 20;
    export const LONG_TIME_WAIT_ELEMENT: number = 30;

    //Url
    export const URL: string = "https://www.agoda.com/";
}