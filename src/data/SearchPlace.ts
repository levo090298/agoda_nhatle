export class SearchPlace {
    place: string;
    checkInDate: Date;
    checkOutDate: Date;
    rooms: number;
    adults: number;
    children: number;
    arr:number[] = [];

    constructor(place: string, checkInDate: Date, checkOutDate: Date, rooms: number, adults: number, children: number, ...items: number[]) {
        this.place = place;
        this.checkInDate = checkInDate;
        this.checkOutDate = checkOutDate;
        this.rooms = rooms;
        this.adults = adults;
        this.children = children;
        this.arr.push(...items);
    }
}   